" vim: set et sw=4 ts=4 sts=4 fdm=marker ff=unix fenc=utf8
"

"------------------------------------------------------------
" vundle - package manager
"------------------------------------------------------------
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
"Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
"Plugin 'L9'
" Git plugin not hosted on GitHub
Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
"Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
"Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line1

"------------------------------------------------------------
" vim package manager
"------------------------------------------------------------
execute pathogen#infect()



"------------------------------------------------------------
" CopyMatches
"------------------------------------------------------------
" Copy matches of the last search to a register (default is the clipboard).
" Accepts a range (default is whole file).
" 'CopyMatches'   copy matches to clipboard (each match has \n added).
" 'CopyMatches x' copy matches to register x (clears register first).
" 'CopyMatches X' append matches to register x.
" We skip empty hits to ensure patterns using '\ze' don't loop forever.
command! -range=% -register CopyMatches call s:CopyMatches(<line1>, <line2>, '<reg>')
function! s:CopyMatches(line1, line2, reg)
	let hits = []
	for line in range(a:line1, a:line2)
		let txt = getline(line)
		let idx = match(txt, @/)
		while idx >= 0
			let end = matchend(txt, @/, idx)
			if end > idx
				call add(hits, strpart(txt, idx, end-idx))
			else
				let end += 1
			endif
			if @/[0] == '^'
				break  " to avoid false hits
			endif
			let idx = match(txt, @/, end)
		endwhile
	endfor
	if len(hits) > 0
		let reg = empty(a:reg) ? '+' : a:reg
		execute 'let @'.reg.' = join(hits, "\n") . "\n"'
	else
		echo 'No hits'
	endif
endfunction

" ----------------------------------------
" The following alternative is to copy matches which extend over more than one line.
" ----------------------------------------
" Use 0"+y0 to clear the clipboard, then
"    :g/pattern/call CopyMultiMatches()
" to copy all multiline hits (just the matching text).
" This is for when the match extends over multiple lines.
" Only the first match from each line is found.
" BUG: When searching for "^function.*\_s*let" the '.*' stops at the end
" of a line, but it greedily skips "\n" in the following (we copy too much).
function! CopyMultiMatches()
	let text = join(getline(".", "$"), "\n") . "\n"
	let @+ .= matchstr(text, @/) . "\n"
endfunction

" Redirect g search output
command! Filter execute 'normal! 0"ay0' | execute 'g//y A' | split | enew | setlocal bt=nofile | put! a

" match path precisely
"map <F8> :NERDTreeToggle <cr>
"map <F7> :TlistToggle<cr>

" Insert timestamp
nmap <F3> a<C-R>=strftime("%Y-%m-%d %a %I:%M %p")<CR><Esc>
imap <F3> <C-R>=strftime("%Y-%m-%d %a %I:%M %p")<CR>

"------------------------------------------------------------
function! CurrectDir()
	let curdir = substitute(getcwd(), "", "", "g")
	return curdir
endfunction
set statusline=\ [File]\ %F%m%r%h\ %w\ \ [PWD]\ %r%{CurrectDir()}%h\ \ %=[Line]\ %l,%c\ %=\ %P


"Tab switch
"-----------------------------------------------------
"ctrl-t : new Tab 
"ctrl-tab : previous Tab / ctrl-PageDown (Xterm) / gt
"ctrl-shift-tab: next Tab / ctrl-PageUp (Xterm) / gT
:nmap <C-tab> :tabnext<cr>
:map <C-tab> :tabnext<cr>
:imap <C-tab> <ESC>:tabnext<cr>i
:nmap <C-S-tab> :tabprevious<cr>
:map <C-S-tab> :tabprevious<cr>
:imap <C-S-tab> <ESC>:tabprevious<cr>i
:nmap <C-t> :tabnew<cr>
:imap <C-t> <ESC>:tabnew<cr>


"================================================================================
" user defined functions
"================================================================================
syntax on
colorscheme torte
"set guifont=Lucida_Console:h15:cANSI
se guifont=Monaco:h14

"set encoding=prc
"set encoding=utf-8
" trying encoding by this order
set fileencodings=ucs-bom,utf-8,gb2312,gbk,cp936,gb18030,big5,euc-jp,euc-kr,latin1
set termencoding=utf-8

set nobackup 
set nowritebackup
set incsearch 
set hlsearch
set ruler       " show the cursor position all the time
set nu 
set smarttab
set ai
set smartindent
set bs=2        " allow backspacing over everything in insert mode
set ts=4 
set sw=4 
set tw=100
set ci
set showmatch

set wildignore=*.o,*.obj,*.bak,*.exe,*.class,*.jar
set tags+=../tags

map <F8> :NERDTreeToggle<CR>
